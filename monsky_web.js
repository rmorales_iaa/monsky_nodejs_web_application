//-----------------------------------------------------------------------------
// Import required modules
const express = require('express');
const { Client } = require('pg');
const http = require('http');
const socketIo = require('socket.io');
//-----------------------------------------------------------------------------
// Create an Express app
const app = express();
const server = http.createServer(app);
//-----------------------------------------------------------------------------
// Create a PostgreSQL client and connect to the database
const dbClient = new Client({
  connectionString: 'postgres://monsky:guinness@localhost:5432/monsky',
});
dbClient.connect();
//-----------------------------------------------------------------------------
// Create a WebSocket server
const io = socketIo(server);
//-----------------------------------------------------------------------------
// Serve your HTML page
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
//-----------------------------------------------------------------------------
// Serve Chart.js library
app.use('/chart.js', express.static(__dirname + '/node_modules/chart.js/dist'));

// Serve the "images" directory
app.use('/images', express.static(__dirname + '/images'));
//-----------------------------------------------------------------------------
// WebSocket logic
io.on('connection', (socket) => {
  console.log('Client connected');

  // Periodically fetch the last 10 entries from the database and send them to the client
   const fetchAndSendData = async () => {
    try {
      const result = await dbClient.query(
        'SELECT * FROM iot_data ORDER BY timestamp DESC LIMIT 10'
      );

      const data = result.rows.reverse(); // Reverse the order to get the latest entries first
      socket.emit('data', data);
    } catch (error) {
      console.error('Error fetching data from the database:', error);
    }
  };

  // Fetch and send data initially and then every 5 seconds
  fetchAndSendData();
  const intervalId = setInterval(fetchAndSendData, 5000);

  // Handle disconnection
  socket.on('disconnect', () => {
    clearInterval(intervalId);
    console.log('Client disconnected');
  });
});
//-----------------------------------------------------------------------------
// Start the server
const port = process.env.PORT || 3000;
server.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
//-----------------------------------------------------------------------------